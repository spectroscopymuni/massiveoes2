from os.path import isfile, getsize
import os
import sqlite3 as sqlite
import pandas, numpy
from scipy.constants import physical_constants, h, c
from scipy.special import wofz
from scipy.signal import fftconvolve


kB = physical_constants['Boltzmann constant in inverse meters per kelvin'][0]
kB /=100 #inverse cm

data_path = os.path.dirname(__file__)
data_path = os.path.join(os.path.split(data_path)[0], 'data')

def has_changed(new_dict, old_dict, rewrite_old_dict = True):
    has_changed = False
    for key in new_dict:
        if old_dict[key] != new_dict[key]:
            has_changed = True
        if rewrite_old_dict:
            old_dict[key] = new_dict[key]
    return has_changed

class wrongInput(Exception):
    pass

class Model(object):

    identifier = 0

    def __init__(self):
        self.input_ = None
        self.output = None
        self.identifier = str(Model.identifier)
        Model.identifier += 1

class AbstractDatabaseLoader(Model):

    def __init__(self):
        Model.__init__(self)
        self.conn = None
        self.db_type = None
        self.sources = None
        self.name = None
        self.input_changed = False
        self.output_changed = False

    def set_up(self, filename):
        self.input_ = filename
        self.get_full_path()
        if self.is_db_file():
            self.connect()
            self.db_type = self.get_metadatum('type')
            self.sources = self.get_sources()
            self.name = self.get_metadatum('name')
            self.input_changed = True
            self.execute()

    def execute(self):
        self.output = self

    def get_full_path(self):
        self.fullpath = os.path.join(data_path, self.input_)
        
    def is_db_file(self):
        if not isfile(self.fullpath):
            return False
        if getsize(self.fullpath) < 100: # SQLite database file header is 100 bytes
            return False
        with open(self.fullpath, 'rb') as fd:
            header = fd.read(100)
        return header[:16] == b'SQLite format 3\x00'

    def connect(self):
        self.conn = sqlite.connect(self.fullpath)

    def get_metadatum(self, which):
        """
        Is it molecular emission, absorption, Raman, atom...?
        """
        curr = self.conn.cursor()
        curr.execute('select '+ which + ' from metadata')
        metadatum = curr.fetchone()[0]
        return metadatum

    def get_sources(self):
        curr = self.conn.cursor()
        curr.execute('select source from metadata')
        return curr.fetchall()

    
class DiatomicDatabaseLoader(AbstractDatabaseLoader):

    def __init__(self):
        AbstractDatabaseLoader.__init__(self)
        self.output_type = 'DiatomicDatabaseLoader'
        
    def execute(self):
        if self.input_changed:
            self.upper_states = pandas.read_sql_query('select * from upper_states',
                                                      self.conn, index_col='id')
            self.lower_states = pandas.read_sql_query('select * from lower_states',
                                                      self.conn, index_col='id')
            self.lines = pandas.read_sql_query('select * from lines',
                                               self.conn, index_col='id')
            self.input_changed = False
            self.output_changed = True
        else:
            self.output_changed = False
        self.output = self

        

class AbstractComputationalModel(Model):

    def __init__(self, workspace):
        Model.__init__(self)
        self.workspace = workspace
        self.paramsdict = {}
        self.kwargs = {}
        self.expected_input_type = None
        self.output_changed = True
        self.kwargs_changed = True
        self.params_changed = True
        
    def update_paramsdict(self):
        has_changed = False
        for key in self.paramsdict:
            if (self.params[key] !=
                self.workspace.params[self.identifier+key].value): 
                self.params[key] = self.workspace.params[self.identifier+key].value
                has_changed = True
        return has_changed
        
    def set_up(self, input_, **kwargs):
        self.check_input(input_)
        self.input_ = input_
        self.kwargs = kwargs
        self.params_changed = self.update_paramsdict()
        self.kwargs_changed = has_changed(kwargs, self.kwargs)

    def execute(self):
        
        if isinstance(self.input_, list):
            for input_instance in self.input_:
                input_instance.execute()
        else:
            self.input_.execute()
    
        if isinstance(self.input_ , list):
            inputs_changed = []
            for _input_item in input_:
                inputs_changed.append(_input_item.output_changed)
            input_changed = any(inputs_changed)
        else:
            input_changed = self.input_.output_changed
                
                
        if ( not self.update_paramsdict()
             and not self.kwargs_changed
             and self.output is not None
             and not input_changed):
            self.output_changed = False
            return self.output
        self._execute()
        return self.output

    def _execute(self):
        """
        To be implemented in each subclass.
        """
        pass

    def check_input(self, input_):
        if isinstance(input_ , list):
            for _input_item in input_:
                try: 
                    if ( (not hasattr(input_, 'output_type')) or
                         self.expected_input_type != input_.output_type):
                        msg = 'DiatomicEmissionModel expects {} input but got {}'.format(self.expected_input_type, type(input_))
                        raise wrongInput(msg)
                except:
                    msg = 'DiatomicEmissionModel expects {} \
                    input but got {}'.format(self.expected_input_type, type(input_))
                    raise wrongInput(msg)
        else:
        
            try: 
                if ( (not hasattr(input_, 'output_type')) or
                     self.expected_input_type != input_.output_type):
                    msg = 'DiatomicEmissionModel expects {} \
                    input but got {}'.format(self.expected_input_type, type(input_))
                    raise wrongInput(msg)
            except:
                msg = 'DiatomicEmissionModel expects {} input but got {}'.format(self.expected_input_type, type(input_))
                raise wrongInput(msg)



class DiatomicEmissionModel(AbstractComputationalModel):


    def __init__(self, workspace=None):

        AbstractComputationalModel.__init__(self, workspace)
        self.kwargs = {'wavelength':'air_wavelength',
                       'y_val_type':'photon_flux'}
        self.norm = None
        self.expected_input_type = 'DiatomicDatabaseLoader'
        self.output_type = 'populated lines'

    def help(self):
        msg = """
        DiatomicEmissionModel.set_up():
        args:
        -----
        input_: DiatomicDatabaseLoader instance
        params: lmfit.Params object with Trot and Tvib params defined
        
        kwargs:
        -------
        wavelength: {'air_wavelength', 'vacuum_wavelenght', 'wavenumber'} 
                    specifies the type of x-axis values.
        y_val_type: {'photon_flux', intensity}
        """
        print(msg)

    def calculate_norm(self, params):
        Trot = params['Trot']
        Tvib  = params['Tvib']
        degeneracy = (2*self.input_.upper_states.J+1)
        parts = degeneracy*numpy.exp(-self.input_.upper_states.E_J /
                (kB*Trot) - self.input_.upper_states.E_v / (kB*Tvib))
        return numpy.sum(parts)
        
    def _execute(self):
        self.norm = self.calculate_norm(self.paramsdict)
        Trot = self.paramsdict['Trot']
        Tvib = self.paramsdict['Tvib']
        pops =  ((2*self.input_.upper_states['J']+1)*
                 numpy.exp(-self.input_.upper_states['E_v']/(kB*Tvib) -
                           self.input_.upper_states['E_J']/(kB*Trot)) ) / self.norm
        merged = self.input_.lines.merge(pops.rename('pops'), left_on='upper_state',
                                right_index=True)
        y = merged['A']*merged['pops']
        if self.kwargs['y_val_type'] == 'intentsity':
            y *= 100*h/c * merged['wavenumber']
        self.output = pandas.DataFrame(
            {'x':merged[self.kwargs['wavelength']],'y':y})


class DiatomicAbsorptionModel(DiatomicDatabaseLoader):
    """
    Not implemented yet
    """

    def __init__(self, workspace=None):
        raise NotImplementedError("sorry, not yet available")
        #DiatomicDatabaseLoader.__init__(self, workspace)
        


class MeshModel(AbstractComputationalModel):

    """
    adds artificial zeros in between lines. Usually used after creating a simulated spectrum before 
    convolution with slit function. 

    return:
    Spectrum objects with pretty many points (or fine mesh, if you preffer)
    """

    def __init__(self, workspace=None):
        self.output_type = 'meshed spectra'
        self.expected_input_type = 'populated lines'
        
    
        
    def _execute(self):
    
        input_ = self.input_
        
        inputs = []
        if isinstance(input_, list):
            for input_instance in input_:
                inputs.append(input_instance.execute())
            self.merged_frames = pandas.concat(self.input_)
        else:
            self.merged_frames = input_
            
        
        start_spec = np.min(self.merged_frames['x']) -  2#prevent lines from falling to edges
        end_spec = np.max(self.merged_frames['x']) + 2
        if not points_per_nm:
            points_per_nm = 1000
     
        no_of_points = int(np.abs(end_spec-start_spec)*points_per_nm)
        
        #print(no_of_points)
        
        spec_x = np.zeros(no_of_points)
        spec_y = np.zeros(no_of_points)
        
        spec_x = np.linspace(start_spec, end_spec, no_of_points)

        for i in range(len(self.merged_frames['x'])):
            index = int((self.merged_frames['x'].iloc[i] - start_spec)*points_per_nm + 0.5)
            spec_y[index] += self.merged_frames['y'].iloc[i]
            
        self.output = {'x': spec_x, 'y': spec_y}



def voigt(x, y):
    """
Taken from `astro.rug.nl <http://www.astro.rug.nl/software/kapteyn-beta/kmpfittutorial.html?highlight=voigt#voigt-profiles/>`_

The Voigt function is also the real part of
`w(z) = exp(-z^2) erfc(iz)`, the complex probability function,
which is also known as the Faddeeva function. Scipy has
implemented this function under the name `wofz()`
    """
    z = x + 1j*y
    I = wofz(z).real
    return I

def Voigt(nu, alphaD, alphaL, nu_0, A, a=0, b=0):
    """
Taken from `astro.rug.nl <http://www.astro.rug.nl/software/kapteyn-beta/kmpfittutorial.html?highlight=voigt#voigt-profiles/>`_

The Voigt line shape in terms of its physical parameters

Args:
  **nu**:  light frequency axis

  **alphaD**:  Doppler broadening HWHM

  **alphaL**:  Lorentzian broadening HWHM

  **nu_0**:  center of the line

  **A**:  integral under the line

  **a**:  constant background

  **b**:  slope of linearly changing background (bg = a + b*nu)

Returns:
  **V**: The voigt profile on the nu axis
    """
    if alphaD == 0:
        alphaD = 1e-10
    if alphaL == 0:
        alphaL = 1e-10
    f = np.sqrt(np.log(2))
    x = (nu-nu_0)/alphaD * f
    y = alphaL/alphaD * f
    backg = a + b*nu
    V = A*f/(alphaD*np.sqrt(np.pi)) * voigt(x, y) + backg
    return V

class VoigtModel(AbstractComputationalModel):

    def __init__(self, workspace=None):
        AbstractComputationalModel.__init__(self, workspace)

    def execute(self, input_, params, **kwargs):
        """
        params:
        -------
        input_: equidistant array

        params: lmfit.Parameters object containing slitf_gauss and 
                slitf_lorentz parameters
        
        kwargs:
        -------
        force: *bool* recalculate even if the input and 
        params did not chage from the last time. Defaults to False.

        """
        self.input_ = input_
        force = kwargs.pop('force', False)
        if ( not self.update_paramsdict()
             and not force
             and self.output is not None):
            return self.output
        
        gauss = params['slitf_gauss']
        lorentz = params['slitf_lorentz']
        slit = Voigt(self.input_['x'], gauss, lorentz,
                     numpy.mean(self.input_['x']), 1)
        slit /= numpy.sum(slit)
        convolved = fftconvolve(self.input_['y'], slit, mode='same')
        if any(numpy.isnan(self.input_['y'])):
            self.y[:] = 1e100
        self.output = convolved
