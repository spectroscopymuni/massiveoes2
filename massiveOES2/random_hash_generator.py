import random 
import string 

# defining function for random 
# string id with parameter 
def ran_hash_gen(size, chars=string.ascii_uppercase + string.digits): 
    return ''.join(random.choice(chars) for x in range(size)) 