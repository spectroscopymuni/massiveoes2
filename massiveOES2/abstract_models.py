import lmfit
import json
from .random_hash_generator import ran_hash_gen
import sys
from numpy import inf

class Node(object):
    identifier = 0
    def __init__(self, **kwargs):
        
        self._downstream_nodes = []
        self._upstream_nodes = []
        self._upstream_changed = False
        identifier = kwargs.pop('identifier', False)
        if not identifier:
            self.identifier = ran_hash_gen(8)
        else:
            self.identifier = identifier
        
    def add_upstream_node(self, node):
        
        if node not in self._upstream_nodes:
            self._upstream_nodes.append(node)
            self._upstream_changed = True
        if self not in node._downstream_nodes:
            node.add_downstream_node(self)
        
    def remove_upstream_node(self, node):
        
        if node in self._upstream_nodes:
            self._upstream_nodes.remove(node)
            self._upstream_changed = True
        if self in node._downstream_nodes:
            node.remove_downstream_node(self)
            
    def add_downstream_node(self, node):
  
        if node not in self._downstream_nodes:
            self._downstream_nodes.append(node)
        if self not in node._upstream_nodes:
            node._upstream_changed = True
            node.add_upstream_node(self)
    
    def remove_downstream_node(self, node):
    
        if node in self._downstream_nodes:
            self._downstream_nodes.remove(node)
        if self in node._upstream_nodes:
            node._upstream_changed = True
            node.remove_upstream_node(self)
            
    def _disconect(self):
    
        for node in self._upstream_nodes:
            node.remove_downstream_node(self)
            
        for node in self._downstream_nodes:
            node.remove_upstream_node(self)
        
    def notify_changes(self):
        
        for node in self._downstream_nodes:
            
            node._upstream_changed = True



        
class Workspace(Node):
    
    def __init__(self, database_repository = False):
        """
        database_repository: prevzato z Bundle, je to seznam vsech SQLite databazi
        """
        super().__init__()
        self.params = WorkspaceParameters()
        self.models = {}
        self.database_repository = database_repository
        self.measured_spectrum = None

    def add_model(self):

        pass

    def add_serialized_model(self, serialized_model, params=False, settings=False):
    
        # vytvorit model do new_model
        model_class = getattr(sys.modules[__name__], serialized_model['classname'])

        # model se sam registruje do WS
        print(model_class)
        new_model = model_class(workspace=self,
                                settings=settings,
                                params=params,
                                identifier=serialized_model['identifier'])



    def remove_model(self, model):

        del self.models[model.identifier]
        model._disconect()


    def serialize_models(self):
        """
        iterate over tree structure of all models all the way down to each leaf
        """
        serialized_models = {}
        for model in self.models:
            serialized_models[model] = self.models[model].serialize()
            
        return serialized_models
            
    def __getstate__(self):
        return {
            'params':self.params.dumps(),
            'measured_spectrum': self.measured_spectrum,
            'models':self.serialize_models() 
            }

    # def __setstate__(self, state):
    #     pass

    @classmethod
    def load(klas, data, database_repository=False):
        """
        args:
        -----
        *data*: 
        """
        new_workspace = klas(database_repository=database_repository)
        new_workspace.params = WorkspaceParameters()
        new_workspace.params.loads(data['params'])
        print(new_workspace.params)
        for serialized_model_key in data['models']:
            serialized_model = data['models'][serialized_model_key]
            new_workspace.add_serialized_model(serialized_model,
                                               settings=serialized_model['settings'],
            )

        for model_key in new_workspace.models:
            model = new_workspace.models[model_key]
            for input_model in data['models'][model.identifier]['inputs']:
                model.addinput_(new_workspace.models[input_model]) 

        return new_workspace

     

class Model(Node):
    
    def __init__(self, workspace, settings = False, params = False, **kwargs):
        """
        input_ is a subset of self._upstream_nodes, serialize as a list 
        of identifiers.

        output is data
        """
        super().__init__(**kwargs)
        self.workspace = workspace
        self.workspace.add_upstream_node(self)
        self.workspace.models[self.identifier] = self
        self.input_ = False
        self.settings = Settings(settings)
        self.settings.add_downstream_node(self)

            
        if params:
            self.params = ModelParams(params, workspace, self.identifier)
            self.params.add_downstream_node(self)
        else:
            self.params = False
            
        self.output = False
            
    def __call__(self):
        if (not self._upstream_changed) and self.output:
            return self.output
            
        else:
            self._execute()
            
        self._upstream_changed = False
        
    def _close(self):
        
        if hasattr(self,'params'):
            self.params.remove_downstream_node(self)
            del self.params
            
        if hasattr(self,'settings'):
            self.settings.remove_downstream_node(self)
            del self.settings
            
        self._disconect()
         
    def _execute(self):
    
        pass
        # write output to self.output

    def get_input_IDs(self):
        raise NotImplemented()
        
        
    def serialize(self):
        dict_out = {
            'inputs' : self.get_input_IDs(), 
            'classname' : str(self.__class__.__name__),
            'settings' : self.settings.settings,
            'identifier':self.identifier,
        }
        return dict_out
        


    def dumps(self):
        """
        convert to a dictionary, then call json.dumps
        """
        dict_out = self.serialize()
        return json.dumps(dict_out)
        
class SingleInputModel(Model):

    def __init__(self, workspace, settings = False, params = False, **kwargs):
        super().__init__(workspace, settings, params, **kwargs)
        
    def addinput_(self, input_):
        
        if self.input_:
            self.input_.remove_downstream_node(self)
        
        self.input_ = input_
        self.input_.add_downstream_node(self)
        
    def removeinput_(self):
    
        if self.input_:
            self.input_.remove_downstream_node(self)
            
        self.input_ = False

    def get_input_IDs(self):
        if self.input_:
            return [self.input_.identifier]
        else:
            return []
        

class MultiInputModel(Model):

    def __init__(self, workspace, settings = False, params = False, **kwargs):
        super().__init__(workspace, settings, params, **kwargs)
        self.input_ = []
        
    def addinput_(self, input_):
    
        if input_ not in self.input_:

            self.input_.append(input_)
            input_.add_downstream_node(self)
        
    def removeinput_(self, input_):
    
        if input_ in self.input_:
            input_.remove_downstream_node(self)
            self.input_.pop(input_,0)

    def get_input_IDs(self):
        if self.input_:
            return [inp.identifier for inp in self.input_]
        else:
            return []


class WorkspaceParameters(lmfit.Parameters):

    def add(self, name, value=None, vary=True, min=-inf,
            max=inf, expr=None,
            brute_step=None):
        if isinstance(name, WorkspaceParameter):
            self.__setitem__(name.name, name)
        else:
            self.__setitem__(name, WorkspaceParameter(value=value, name=name, vary=vary,
                                             min=min, max=max, expr=expr,
                                             brute_step=brute_step))
    def loads(self, s, **kws):
        prms = super().loads(s,**kws)
        for key in self.keys():
            self[key] = WorkspaceParameter.from_lmfit_parameter(self[key])
            #self.__setitem__(key, WorkspaceParameter.from_lmfit_parameter(
            #    self.__getitem__(key)))
        
            
class WorkspaceParameter(lmfit.Parameter):


    @classmethod
    def from_lmfit_parameter(klas, parameter):
        ret = klas(parameter.name,
                   value=parameter.value,
                   vary=parameter.vary,
                   min=parameter.min, max=parameter.max,
                   expr=parameter.expr,
                   brute_step=parameter.brute_step,
                   user_data=parameter.user_data)
        return ret
    
    @property
    def value(self):
        """Return the numerical value of the Parameter, with bounds applied."""
        return super()._getval()
    
    @value.setter
    def value(self, val):
        if val != super()._getval():
            self.on_change()
        lmfit.Parameter.value.fset(self, val)
        #self.value.fset(self, val)

    def on_change(self):
        """
        lazy!
        V okamziku zmeny se vsechny parametry vsech modelu maji podivat
        na hodnoty parametru na Workspacu a kdyztak se aktualizovat
        """
        print("parameter {} value changed!".format(self.name))
            
class ModelParams(Node):
    
    def __init__(self, paramsdict, workspace, belongs_to):
        self.belongs_to = belongs_to
        super().__init__(identifier = belongs_to + '_params')
        ModelParams.check_paramsdict(paramsdict)
        self.paramsdict = paramsdict
        self.workspace = workspace
        self._register_in_workspace()


    @staticmethod
    def check_paramsdict(paramsdict):
        for key in paramsdict:
            try:
                res = key+'string'
            except TypeError:
                raise TypeError(__name__+'.ModelParams: paramsdict keys must be strings!')
        
    def _register_in_workspace(self):
        
        for key in self.paramsdict:
            print('registering ' + key)
            if key+self.identifier not in self.workspace.params:
                self.workspace.params.add(key+self.identifier,
                                          value = self.paramsdict[key])
            else:
                self.paramsdict[key] = self.workspace.params[key+self.identifier].value
                
    
    def _update_from_workspace(self):
        """
        compare self.paramsdict with workspace parameters, if differs
        notify_changes.
        """
        changed = False
        
        for key in self.paramsdict:
            if self.workspace.params[key+self.identifier].value != self.paramsdict[key]:
                self.paramsdict[key] = self.workspace.params[key+self.identifier].value
                changed = True
                
        if changed:
            self.notify_changes()
            
    def _remove_from_workspace(self):
    
        for key in self.paramsdict:
            self.workspace.params.pop(key+self.identifier,0)

    def __getitem__(self, key):
        return self.paramsdict.__getitem__(key)

    def __setitem__(self, key, value):
        raise RuntimeError("Please, don't overwrite the parameters this way!")
        
          
class Settings(Node):
    
    def __init__(self,settings):
        super().__init__()
        self.settings = settings
        
    def __setitem__(self, key, value):
        self.notify_changes()
        self.settings.__setitem__(key, value)
        
    def __getitem__(self, key):
        return self.settings.__getitem__(key)



class TestModel(MultiInputModel):

    def __init__(self,workspace,**kwargs):
        params = kwargs.pop('params', False)
        if not params:
            params = {'amplitude':10,
                      'tau':1e-4}
        settings = kwargs.pop('settings', False)
        if not settings:
            settings = {'packa':'nahore',
                        'cudlik':'dole'}
        super().__init__(workspace, settings = settings, params = params,
                         **kwargs)


