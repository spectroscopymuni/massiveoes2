##package for batch processing of optical emission spectra
##Authors: Jan Vorac and Petr Synek
##contact: vorac@mail.muni.cz, synek@physics.muni.cz

from setuptools import setup, find_packages
setup(
    name = "massiveOES",
    version = "2.0 - alpha",
    packages = find_packages(),
    install_requires=['lmfit>=0.9.13' , 'scipy>=1.3.0', 'numpy>=1.16.4', 'asteval>=0.9.14', 'pandas>=0.24.2']
)
